﻿using System;

// 考虑书中向 3-结点插入结点的所有情况示意图，再加上允许右侧红链接的情况（左侧红链接的情况作轴对称变换）即可。
// 这个示意图位于中文版图 3.3.20，英文版 P436 插图 Insert into a single 3-node (three cases)
Console.WriteLine("see /docs/content/3-3-8.md for diagrams");